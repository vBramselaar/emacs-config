;;; windows.el --- Emacs configuration for Windows

;;; Commentary:
;;; Only require this config when on Windows.

;;; Code:

;; No newline at end of file
(setq require-final-newline nil)
(setq mode-require-final-newline nil)

(setq flymake-no-changes-timeout nil)

(use-package powershell
  :ensure t)

(setenv "PATH" (concat
                "C:\\Program Files\\Git\\usr\\bin" ";"
                (getenv "PATH")))

(setq ediff-diff-program "C:/Program Files/Git/usr/bin/diff.exe")
(setq ediff-diff3-program "C:/Program Files/Git/usr/bin/diff3.exe")

(use-package ob-powershell
  :ensure t
  :after (org)
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((powershell . t))))

;; Fixes Windows NUL device error for now
(setq grep-use-null-device nil)

(provide 'windows)
;;; windows.el ends here
