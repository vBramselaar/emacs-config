;;; modeline.el --- Emacs Modeline configuration

;;; Commentary:

;; Settings related to Modeline.

;;; Code:

;; Flat modeline style
(defvar mode-line-height 4)

(defun flat-style(theme &rest args)
  "Make modeline and its borders the same colour (unused THEME ARGS)."
  (custom-set-faces
   `(mode-line
     ((t (:inherit mode-line
                   :box (:line-width ,mode-line-height :style flat-button)))) t)
   `(mode-line-inactive
     ((t (:inherit mode-line-inactive
                   :box (:line-width ,mode-line-height :style flat-button)))) t)))

(advice-add 'enable-theme :after #'flat-style)

;; Modeline format
(defvar my-mode-line-position
  '("("
    "%02l"
    ","
    "%02c"
    ")"))

(defvar my-mode-line-buffer
  '(:eval (propertize "%b " 'face '(bold font-lock-keyword-face)
                      'help-echo (buffer-file-name))))

(defvar my-mode-line-major-mode
  '("["
    (:eval (propertize (if (listp mode-name) (car mode-name) mode-name) 'face 'font-lock-string-face))
    "]"))

(defface error-notbold-face '((t :inherit error :weight normal))
  "Error colour without being bold.")

(defvar my-mode-line-modified
  '(:eval (if (buffer-modified-p)
              (propertize "●"
                          'face 'error-notbold-face
                          'help-echo "Buffer has been modified")
            (propertize "●"
                        'face 'shadow
                        'help-echo "No modifications to buffer"))))

(defvar my-mode-line-coding-format
  '(:eval
    (let* ((code (symbol-name buffer-file-coding-system))
           (eol-type (coding-system-eol-type buffer-file-coding-system))
           (eol (cond
                 ((eq 0 eol-type)
                  "LF")
                 ((eq 1 eol-type)
                  "CR/LF")
                 ((eq 2 eol-type)
                  "CR")
                 (t
                  "???"))))
      (concat code ":" eol))))

(defvar my-mode-line-linter
  '(:eval (when (bound-and-true-p flymake-mode) flymake-mode-line-format)))

(when (version< emacs-version "30")
  (defvar mode-line-format-right-align "")
  (defvar mode-line-right-align-edge nil))

(setopt mode-line-right-align-edge 'right-margin)

(setq-default mode-line-format
              `("%e"
                mode-line-front-space
                ,my-mode-line-modified " "
                mode-line-client mode-line-remote
                mode-line-frame-identification
                ,my-mode-line-buffer " "
                ,my-mode-line-position " "
                ,my-mode-line-linter " "
                mode-line-misc-info
                mode-line-format-right-align
                ,my-mode-line-major-mode " "
                ,my-mode-line-coding-format))

(provide 'modeline)
;;; modeline.el ends here
