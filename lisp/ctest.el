;;; ctest.el --- Simple test runner for CTest

;;; Commentary:


;;; Code:
(require 'project)

(defgroup ctest nil
  "Simple test runner for CTest"
  :group 'programming)

(defcustom ctest-project-relative-test-dir "build"
  "CTest --test-dir option relative to project root."
  :group 'ctest
  :type 'string
  :safe #'stringp)

(defcustom ctest-parallel-amount 4
  "Amount of cores to run CTest parallel."
  :group 'ctest
  :type 'natnum
  :safe #'natnump)

(defun ctest-json-to-names (json)
  "Convert CTest JSON output to a list of test names."
  (mapcar (lambda (test)
            (gethash "name" test))
          (gethash "tests" json)))

(defun ctest-query-tests-json (test-dir)
  "Query a JSON hash-table of all tests in TEST-DIR."
  (shell-command-to-string (concat "ctest --test-dir \"" test-dir "\" --show-only=json-v1")))

(defun ctest-make-regex-command (test-dir regex)
  "Make a CTest command that will run from TEST-DIR with REGEX."
  (concat "ctest --parallel " (number-to-string ctest-parallel-amount) " --output-on-failure --test-dir \"" test-dir "\" -R \"" regex "\""))

(defun ctest-test-dir ()
  "Return the relevant CTest test-dir."
  (concat (project-root (project-current)) ctest-project-relative-test-dir))

(defun ctest-run (regex)
  "Run CTest tests matching REGEX."
  (interactive
   (let* ((tests-json (json-parse-string (ctest-query-tests-json (ctest-test-dir))))
          (names (ctest-json-to-names tests-json)))
     (list
      (completing-read "CTest regex query: " names))))
  (let ((outbuffer (get-buffer-create "*ctest*"))
        (command (ctest-make-regex-command (ctest-test-dir) regex)))
    (async-shell-command command outbuffer)
    (with-current-buffer outbuffer (view-mode))))

(provide 'ctest)
;;; ctest.el ends here.
