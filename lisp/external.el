;;; external.el --- Emacs external packages

;;; Commentary:

;; Settings related to external dependencies

;;; Code:
(use-package package
  :config
  (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
  (setq package-archive-priorities
        '(("gnu" . 10)
          ("nongnu" . 8)
          ("melpa-stable" . 5)
          ("melpa" . 0))))

(use-package whole-line-or-region
  :ensure t
  :config
  (whole-line-or-region-global-mode 1))

(when (version< emacs-version "30")
  (use-package which-key
    :ensure t
    :config
    (setq which-key-show-early-on-C-h t)
    (which-key-mode)))

(use-package eat
  :ensure t
  :config
  (setq-default eat-kill-buffer-on-exit t)
  :hook ((eshell-load . eat-eshell-mode)
         (eshell-load . eat-eshell-visual-command-mode)))

(use-package magit
  :ensure t
  :config
  (setq magit-define-global-key-bindings nil)
  :bind (("C-x g s" . magit-status)
         ("C-x g f" . magit-file-dispatch)
         ("C-x g c" . magit-dispatch)))

(use-package eldoc-box
  :ensure t
  :config
  (setq eldoc-box-offset '(16 32 26))
  (setq eldoc-box-clear-with-C-g t)
  :hook (eglot-managed-mode . eldoc-box-hover-mode))

(use-package cmake-mode
  :ensure t)

(use-package lua-mode
  :defer t)

(use-package haskell-mode
  :hook (haskell-mode . eglot-ensure))

(use-package ada-ts-mode
  :custom (ada-ts-mode-indent-backend 'lsp)
  :init
  (with-eval-after-load 'eglot
    (add-to-list 'eglot-server-programs '(ada-ts-mode . ("ada_language_server"))))
  :config
  (add-to-list 'major-mode-remap-alist '(ada-mode . ada-ts-mode))
  :hook (ada-ts-mode . eglot-ensure))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode)))

(use-package adoc-mode
  :defer t
  :config
  (setq adoc-max-image-size '(640 . 480)))

(use-package paredit
  :ensure t
  :hook ((emacs-lisp-mode . enable-paredit-mode)
         (scheme-mode . enable-paredit-mode)
         (clojure-mode . enable-paredit-mode)
         (lisp-mode . enable-paredit-mode)))

(use-package geiser-guile
  :ensure t
  :hook (scheme-mode . geiser-mode))

(use-package sly
  :ensure t
  :config
  (setq inferior-lisp-program "sbcl"))

(when (and (display-graphic-p)
           (not (eq system-type 'windows-nt)))
  (use-package pdf-tools
    :ensure t
    :mode  ("\\.pdf\\'" . pdf-view-mode)
    :config
    (setq-default pdf-view-display-size 'fit-page)
    (setq pdf-annot-activate-created-annotations t)
    (pdf-loader-install)))

(use-package tex
  :ensure auctex
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq TeX-PDF-mode t)
  (setq reftex-plug-into-AUCTeX t)
  (add-to-list 'TeX-view-program-selection '(output-pdf "PDF Tools"))
  :hook ((LaTeX-mode-hook . LaTeX-math-mode)
         (LaTeX-mode-hook . turn-on-reftex)
         (TeX-after-compilation-finished-functions . TeX-revert-document-buffer)))

(use-package elfeed
  :ensure t
  :config
  (setq-default elfeed-search-filter "@3-days-ago")
  (setq feed-path (concat user-emacs-directory "feeds.opml"))
  (when (file-exists-p feed-path)
    (elfeed-load-opml feed-path)))

(use-package smart-tabs-mode
  :config
  (smart-tabs-insinuate 'c 'c++))

(provide 'external)
;;; external.el ends here
