;; -*- coding: utf-8; lexical-binding: t -*-
;;; init.el --- Emacs configuration init

;;; Code:

;; Minimize garbage collection during startup
(setq gc-cons-threshold most-positive-fixnum)

;; Lower threshold back to 8 MiB (default is 800kB)
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold (expt 2 23))))

;; Path to config files
(add-to-list 'load-path (expand-file-name (concat user-emacs-directory "/lisp/")))

;; Load modeline settings
(require 'modeline)

(use-package emacs
  :demand t
  :config
  ;; Seperate custom file
  (setq custom-file (concat user-emacs-directory "/custom.el"))
  ;; Backup files location
  (setq backup-directory-alist `(("." . ,(concat user-emacs-directory "/backup"))))
  ;; Disable auto-save
  (setq auto-save-default nil)
  ;; Set startup window size
  (setq default-frame-alist
        '((width . 100) (height . 40)))
  ;; Non-choppy resizing
  (setq frame-resize-pixelwise t)
  ;; Disable startup screen if any argument is passed
  (when (cdr command-line-args)
    (setq inhibit-startup-screen t))
  ;; Disable toolbar and scrollbars
  (when (display-graphic-p)
    (tool-bar-mode -1)
    (scroll-bar-mode -1)
    (horizontal-scroll-bar-mode -1))
  ;; Disable beep
  (setq ring-bell-function #'ignore)
  ;; Use bar cursor instead of block
  (setq-default cursor-type 'bar)
  ;; Scrolling
  (setq fast-but-imprecise-scrolling t)
  (pixel-scroll-precision-mode 1)
  (setq mouse-wheel-scroll-amount '(0.05 ((shift) . 0.2) ((control) . nil)))
  (setq mouse-wheel-progressive-speed nil)
  (setq scroll-conservatively 101)
  ;; Horizontal scrolling
  (setq mouse-wheel-tilt-scroll t)
  (setq mouse-wheel-flip-direction t)
  ;; Don't wrap lines
  (when (display-graphic-p)
    (setq-default truncate-lines t))
  ;; Use y and n instead of yes/no
  (setq use-short-answers t)
  ;; Disable confirm kill process
  (setq confirm-kill-processes nil)
  ;; Disable warnings
  (setq native-comp-async-report-warnings-errors 'silent)
  ;; Delete selection on yank
  (delete-selection-mode)
  ;; Tab width and use spaces
  (setq-default tab-width 4)
  (setq-default indent-tabs-mode nil)
  ;; Enable indentation+completion using the TAB key.
  (setopt tab-always-indent 'complete)
  ;; Hide commands in M-x which do not apply to the current mode.
  (setq read-extended-command-predicate #'command-completion-default-include-p)
  ;; Treats manual buffer switching the same as programmatic switching.
  (setq switch-to-buffer-obey-display-actions t)
  ;; Use view-mode for read-only files
  (setq view-read-only t)
  ;; Highlight current-line
  (global-hl-line-mode 1)
  ;; Set font
  (if (eq system-type 'windows-nt)
      (set-frame-font "Consolas 11" nil t)
    (set-frame-font "Hack 10" nil t))

  ;; Colour theme
  (defvar current-theme 'modus-operandi)
  (defvar alt-theme 'modus-vivendi)
  (load-theme current-theme t t)
  (load-theme alt-theme t t)
  (enable-theme current-theme)

  (defun my-toggle-theme ()
    (interactive)
    (let ((former-theme (car custom-enabled-themes)))
      (enable-theme alt-theme)
      (disable-theme current-theme)
      (setq current-theme alt-theme)
      (setq alt-theme former-theme)))

  (defun move-line-up ()
    (interactive)
    (unless (<= (line-beginning-position) 1)
      (let ((column (current-column)))
        (transpose-lines 1)
        (previous-line 2)
        (move-to-column column))))

  (defun move-line-down ()
    (interactive)
    (let ((column (current-column)))
      (next-line 1)
      (transpose-lines 1)
      (previous-line 1)
      (move-to-column column)))

  :bind (("M-<up>" . move-line-up)
         ("M-<down>" . move-line-down)
         ("<f8>" . my-toggle-theme)))

(require 'ctest)

(defun profile-10seconds ()
  (interactive)
  (profiler-start 'cpu)
  (run-with-timer 10 nil (lambda ()
                           (profiler-stop)
                           (profiler-report))))

(use-package so-long
  :config
  (global-so-long-mode 1))

(use-package which-key
  :unless (version< emacs-version "30")
  :config
  (setq which-key-show-early-on-C-h t)
  (which-key-mode))

(use-package display-line-numbers
  :hook (prog-mode . display-line-numbers-mode))

;; Allman-style c style
(use-package cc-vars
  :config
  (setq c-default-style "bsd")
  (setq c-basic-offset 4)
  ;; TAB complete-at-point fix
  (setq c-tab-always-indent nil)
  (setq c-insert-tab-function 'indent-for-tab-command))

(use-package treesit
  :if nil ;;(treesit-available-p)
  :custom (treesit-font-lock-level 3)
  :config
  (setq treesit-language-source-alist
        '((cpp "https://github.com/tree-sitter/tree-sitter-cpp")
          (c "https://github.com/tree-sitter/tree-sitter-c")))
  (when (and (treesit-language-available-p 'cpp)
             (treesit-language-available-p 'c))
    (add-to-list 'major-mode-remap-alist '(c-mode . c-ts-mode))
    (add-to-list 'major-mode-remap-alist '(c++-mode . c++-ts-mode))
    (add-to-list 'major-mode-remap-alist '(c-or-c++-mode . c-or-c++-ts-mode)))
  (setq c-ts-mode-indent-offset c-basic-offset)
  (setq c-ts-mode-indent-style (intern c-default-style)))

(use-package flymake
  :config
  (setq flymake-mode-line-lighter t)
  :hook (prog-mode . flymake-mode))

(use-package eglot
  :hook ((c++-mode c++-ts-mode
          c-mode c-ts-mode
          csharp-mode csharp-ts-mode) . eglot-ensure)
  :config
  (setq eglot-events-buffer-size 0)
  (when (eq system-type 'windows-nt)
    (let ((homedir (expand-file-name (getenv "USERPROFILE"))))
      (add-to-list 'exec-path (concat homedir "/AppData/Local/Programs/omnisharp"))
      (add-to-list 'exec-path (concat homedir "/AppData/Local/Programs/LLVM/bin")))))

(use-package ibuffer
  :config
  (setq ibuffer-saved-filter-groups
        (quote (("default"
                 ("Dired" (mode . dired-mode))
                 ("Erc" (mode . erc-mode))
                 ("Emacs" (name . "^\\*.*\\*$")) ))))
  :bind ("C-x C-b" . ibuffer)
  :hook ((ibuffer-mode . (lambda ()
                           (ibuffer-switch-to-saved-filter-groups "default")
                           (setq ibuffer-hidden-filter-groups (list "Dired" "Emacs"))
                           (ibuffer-update nil t)))))

(use-package mouse
  :config
  (context-menu-mode 1))

(use-package whitespace
  :config
  (setq-default whitespace-style '(face spaces tabs trailing space-mark tab-mark))
  (defun reload-whitespace-colours (theme &rest args)
    (set-face-attribute 'whitespace-tab nil
                        :foreground (face-attribute 'line-number :foreground)
                        :background (face-attribute 'default :background))
    (set-face-attribute 'whitespace-space nil
                        :foreground (face-attribute 'line-number :foreground)
                        :background (face-attribute 'default :background)))
  (advice-add 'enable-theme :after #'reload-whitespace-colours)
  (reload-whitespace-colours current-theme)
  :hook (prog-mode . whitespace-mode))

(use-package compile
  :config
  (setq compilation-scroll-output t)
  (add-to-list 'display-buffer-alist
               '("\\*compilation\\*"
                 (display-buffer-reuse-window
                  display-buffer-pop-up-window)
                 (reusable-frames . visible))))

(use-package ansi-color
  :hook (compilation-filter . ansi-color-compilation-filter))

;; https://www.doof.me.uk/2019/06/09/making-emacs-gud-usable/
(use-package gud
  :config
  (setq gdb-many-windows t)
  (setq gdb-use-separate-io-buffer t)
  (advice-add 'gdb-setup-windows :after
              (lambda () (set-window-dedicated-p (selected-window) t)))
  (defconst gud-window-register 123456)
  (defun gud-quit ()
    (interactive)
    (gud-basic-call "quit"))
  (add-hook 'gud-mode-hook
            (lambda ()
              (gud-tooltip-mode)
              (window-configuration-to-register gud-window-register)
              (local-set-key (kbd "C-q") 'gud-quit)))
  (advice-add 'gud-sentinel :after
              (lambda (proc msg)
                (when (memq (process-status proc) '(signal exit))
                  (jump-to-register gud-window-register)
                  (bury-buffer)))))

(use-package icomplete
  :demand t
  :config
  (setopt enable-recursive-minibuffers t)
  (setopt completion-auto-help 'always)
  (setopt completions-max-height 20)
  (setopt completions-format 'one-column)
  (setopt completion-auto-select (if (version< emacs-version "30") 'second-tab t))
  (setq completion-styles '(basic initials substring flex))
  (setq read-file-name-completion-ignore-case t)
  (setq read-buffer-completion-ignore-case t)
  (setq icomplete-show-matches-on-no-input t)
  (icomplete-vertical-mode 1)
  :bind (:map icomplete-minibuffer-map
              ("<tab>" . icomplete-force-complete)))

(use-package completion-preview
  :unless (version< emacs-version "30")
  :config
  (setq completion-preview-minimum-symbol-length 2)
  :hook (((text-mode prog-mode) . completion-preview-mode)))

(use-package autorevert
  :demand t
  :config
  (setq global-auto-revert-non-file-buffers t)
  (global-auto-revert-mode 1))

(use-package uniquify
  :demand t
  :config
  (setq uniquify-buffer-name-style 'forward)
  (setq uniquify-separator "/")
  (setq uniquify-after-kill-buffer-p t)
  (setq uniquify-ignore-buffers-re "^\\*"))

(use-package dired-x
  :demand t
  :config
  (setq dired-recursive-copies 'top)
  (setq dired-recursive-deletes 'top)
  (setq dired-kill-when-opening-new-dired-buffer t)
  (setq dired-listing-switches "-alvr")
  :bind (:map dired-mode-map
              ("<backspace>" . dired-up-directory)))

(use-package tab-bar
  :config
  (tab-bar-mode 1)
  (add-to-list 'display-buffer-alist
               '((or (derived-mode . org-mode)
                     (derived-mode . org-agenda-mode))
                 display-buffer-in-tab
                 (tab-name . "Org Notes")
                 (tab-group . "Org")))
  (add-to-list 'display-buffer-alist
               '((derived-mode . erc-mode)
                 display-buffer-in-tab
                 (tab-name . "IRC")
                 (tab-group . "IRC"))))

(use-package eshell
  :config
  (require 'em-smart)
  (setq eshell-where-to-jump 'begin)
  (setq eshell-review-quick-commands nil)
  (setq eshell-smart-space-goes-to-end t)

  (defun open-close-eshell()
    "Open and close eshell with the same command."
    (interactive)
    (cond
     ((string-prefix-p "*eshell*" (buffer-name))
      (kill-buffer-and-window))
     (t
      (split-window-vertically)
      (other-window 1)
      (split-window-vertically)
      (other-window -1)
      (delete-window)
      (other-window 1)
      (eshell 'N))))
  :bind ("<f4>" . open-close-eshell))

(use-package erc
  :commands (erc-tls erc)
  :config
  (setq erc-join-buffer 'buffer)
  (setq erc-kill-buffer-on-part t)
  (setq erc-kill-queries-on-quit t)
  (setq erc-server "irc.libera.chat")
  (setq erc-server-history-list '("irc.snoonet.org"
                                  "irc.rizon.net"))
  (setq erc-interpret-mirc-color t))

(use-package doc-view
  :defer t
  :config
  (setq doc-view-continuous t)
  (setq doc-view-imenu-enabled t))

(use-package epa
  :config
  (setq epa-pinentry-mode 'loopback))

(use-package windmove
  :bind (("C-x C-<left>" . windmove-left)
         ("C-x C-<right>" . windmove-right)
         ("C-x C-<up>" . windmove-up)
         ("C-x C-<down>" . windmove-down)))

(use-package winner
  :config
  (winner-mode 1))

(use-package ediff
  :commands (ediff ediff3)
  :config
  (setq ediff-split-window-function 'split-window-horizontally)
  (setq ediff-window-setup-function 'ediff-setup-windows-plain))

(use-package org
  :defer t
  :config
  (setq-default org-enforce-todo-dependencies t)
  (setq org-src-preserve-indentation t)
  (setq org-hide-emphasis-markers t)
  (setq org-return-follows-link t)
  (setq org-support-shift-select t)
  (setq org-adapt-indentation t)
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.4))
  (setq org-agenda-view-columns-initially t)
  (setq org-columns-default-format
        "%CATEGORY %TODO %3PRIORITY %50ITEM %SCHEDULED %DEADLINE")
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((python . t)
     (C . t)
     (shell . t)
     (scheme . t)))
  :hook (org-mode . visual-line-mode))

(use-package flyspell
  :unless (eq system-type 'windows-nt)
  :config
  (setq flyspell-issue-message-flag nil)
  (setq ispell-dictionary "en_GB")
  :custom-face
  (flyspell-incorrect ((t (:underline (:color "#8d75c0" :style wave)))))
  (flyspell-duplicate ((t (:underline (:color "#50fa7b" :style wave)))))
  :hook
  ((prog-mode . flyspell-prog-mode)
   ((org-mode yaml-mode markdown-mode git-commit-mode) . flyspell-mode)))

(use-package gnus
  :commands (gnus)
  :config
  (setq gnus-select-method '(nnnil nil))
  (setq gnus-use-cache t)
  (setq gnus-read-active-file 'some)
  (setq gnus-summary-thread-gathering-function 'gnus-gather-threads-by-subject)
  (setq gnus-thread-hide-subtree t)
  (setq gnus-thread-ignore-subject t)
  (setq gnus-thread-sort-functions
        '(gnus-thread-sort-by-most-recent-date
          (not gnus-thread-sort-by-number)))
  (setq gnus-summary-line-format (concat "(%d) " gnus-summary-line-format))
  :hook (gnus-article-mode . visual-line-mode))

(use-package hi-lock
  :config
  (global-hi-lock-mode 1)
  (setq hi-lock-file-patterns-policy #'(lambda (dummy) t)) )

(require 'external)

(when (eq system-type 'windows-nt)
  (require 'windows))

;; Local file for system specific settings
(when (file-exists-p (concat user-emacs-directory "/lisp/local.el"))
  (require 'local))

;;; init.el ends here
